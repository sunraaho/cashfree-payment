package com.raaho.payment.service;

import com.raaho.payment.config.CashfreePaymentException;
import com.raaho.payment.model.BeneficiaryRequestModel;

public interface CashfreePaymentService {

	public String authenticate() throws CashfreePaymentException;

	public String addBeneficiary(final String token, final BeneficiaryRequestModel requestModel)
			throws CashfreePaymentException;

	public int addBulkBeneficiaries(final String token) throws CashfreePaymentException;
}
