package com.raaho.payment.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bookgo.payments.persistence.model.SupplierACDetails;
import com.bookgo.payments.persistence.repository.SupplierRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.raaho.payment.config.CashfreePaymentException;
import com.raaho.payment.constants.PaymentConstants;
import com.raaho.payment.model.BeneficiaryRequestModel;
import com.raaho.payment.service.CashfreePaymentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CashfreePaymentServiceImpl implements CashfreePaymentService {

	@Value("${cashfree.payout.test.url}")
	private String payoutTestUrl;

	@Value("${cashfree.client.id}")
	private String clientId;

	@Value("${cashfree.client.secret}")
	private String clientSecret;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private SupplierRepository supplierRepository;

	final ObjectMapper objectMapper = new ObjectMapper();
	final ModelMapper modelMapper = new ModelMapper();

	@SuppressWarnings("unchecked")
	public String authenticate() throws CashfreePaymentException {
		String token = null;

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Client-Id", clientId);
		headers.set("X-Client-Secret", clientSecret);

		HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

		String url = payoutTestUrl + "/payout/v1/authorize";

		Map<String, Object> responseBody = (Map<String, Object>) exchange(url, HttpMethod.POST, requestEntity,
				Map.class).getBody();

		if (responseBody == null) {
			throw new CashfreePaymentException("Empty response body");
		}

		String message = (String) responseBody.get(PaymentConstants.MESSAGE);
		String statusCode = (String) responseBody.get(PaymentConstants.STATUS_CODE);
		String status = (String) responseBody.get(PaymentConstants.STATUS);

		if (PaymentConstants.SUCCESS.equals(status)) {
			Object data = responseBody.get(PaymentConstants.DATA);
			token = data == null ? null
					: (String) objectMapper.convertValue(data, Map.class).get(PaymentConstants.TOKEN);

			if (data == null || token == null) {
				throw new CashfreePaymentException(statusCode, status, "Token not generated :: " + message);
			}

		} else {
			throw new CashfreePaymentException(statusCode, status, message);
		}

		log.info("Authentication Successful :: {}", responseBody);
		log.info("Token :: {}", token);

		return token;
	}

	@SuppressWarnings("unchecked")
	public String addBeneficiary(final String token, final BeneficiaryRequestModel requestModel)
			throws CashfreePaymentException {
		// String token = authenticate();
		// requestModel = getBeneficiaryRequestModel();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token);
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		HttpEntity<BeneficiaryRequestModel> requestEntity = new HttpEntity<>(requestModel, headers);

		String url = payoutTestUrl + "/payout/v1/addBeneficiary";

		Map<String, Object> responseBody = (Map<String, Object>) exchange(url, HttpMethod.POST, requestEntity,
				Map.class).getBody();

		if (responseBody == null) {
			throw new CashfreePaymentException("Empty response body");
		}

		String message = (String) responseBody.get(PaymentConstants.MESSAGE);
		String statusCode = (String) responseBody.get(PaymentConstants.STATUS_CODE);
		String status = (String) responseBody.get(PaymentConstants.STATUS);

		if (PaymentConstants.SUCCESS.equals(status)) {
			log.info(message);
			return status;
		} else {
			throw new CashfreePaymentException(statusCode, status, message);
		}
	}

	private ResponseEntity<?> exchange(final String url, final HttpMethod method, final HttpEntity<?> requestEntity,
			Class<?> responseType) {
		return restTemplate.exchange(url, method, requestEntity, responseType);
	}

	public int addBulkBeneficiaries(final String token) throws CashfreePaymentException {
		// String token = authenticate();
		List<SupplierACDetails> verifiedSupplierACDetails = supplierRepository
				.findAllByIsVerifedTrueAndIsRegisteredFalse();

		int noOfBeneficiariesAdded = 0;

		if (!verifiedSupplierACDetails.isEmpty()) {
			Map<BeneficiaryRequestModel, SupplierACDetails> beneficiaryRequestModelMap = prepareBeneficiaryRequestModelList(
					verifiedSupplierACDetails);
			for (Entry<BeneficiaryRequestModel, SupplierACDetails> beneficiaryRequestModelEntry : beneficiaryRequestModelMap
					.entrySet()) {
				String status = addBeneficiary(token, beneficiaryRequestModelEntry.getKey());
				if (PaymentConstants.SUCCESS.equals(status)) {
					SupplierACDetails updatedSupplierACDetails = beneficiaryRequestModelEntry.getValue();
					updatedSupplierACDetails.setRegistered(true);
					supplierRepository.save(updatedSupplierACDetails);
					noOfBeneficiariesAdded++;
				}
			}
		}
		return noOfBeneficiariesAdded;
	}

	private Map<BeneficiaryRequestModel, SupplierACDetails> prepareBeneficiaryRequestModelList(
			List<SupplierACDetails> verifiedSupplierACDetails) {
		Map<BeneficiaryRequestModel, SupplierACDetails> beneficiaryRequestModelMap = new HashMap<>();
		for (SupplierACDetails supplierACDetails : verifiedSupplierACDetails) {
			String beanId = supplierACDetails.getSupplierUUID().toString();
			BeneficiaryRequestModel beneficiaryRequestModel = new BeneficiaryRequestModel();
			beneficiaryRequestModel.setBeneId(beanId);
			beneficiaryRequestModel.setName(supplierACDetails.getAccountName());
			beneficiaryRequestModel.setPhone(supplierACDetails.getNotificationNumber());
			beneficiaryRequestModel.setEmail(beanId + "@gmail.com");
			beneficiaryRequestModel.setBankAccount(supplierACDetails.getAccountNumber());
			beneficiaryRequestModel.setIfsc(supplierACDetails.getIfscCode());
			beneficiaryRequestModel.setAddress1(supplierACDetails.getAddress());

			beneficiaryRequestModelMap.put(beneficiaryRequestModel, supplierACDetails);
		}
		return beneficiaryRequestModelMap;
	}

	/*
	 * private BeneficiaryRequestModel getBeneficiaryRequestModel() { Map<String,
	 * String> beneficiaryMap = new HashMap<>();
	 * 
	 * beneficiaryMap.put("beneId", "JOHN18011345"); beneficiaryMap.put("name",
	 * "john doe"); beneficiaryMap.put("email", "johndoe@cashfree.com");
	 * beneficiaryMap.put("phone", "9876543210"); beneficiaryMap.put("bankAccount",
	 * "00111122233"); beneficiaryMap.put("ifsc", "HDFC0000001");
	 * beneficiaryMap.put("address1", "ABC Street"); beneficiaryMap.put("city",
	 * "Bangalore"); beneficiaryMap.put("state", "Karnataka");
	 * beneficiaryMap.put("pincode", "560001");
	 * 
	 * BeneficiaryRequestModel beneficiaryRequestModel = new
	 * BeneficiaryRequestModel(); modelMapper.map(beneficiaryMap,
	 * beneficiaryRequestModel);
	 * 
	 * return beneficiaryRequestModel; }
	 */

}
