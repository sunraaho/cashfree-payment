package com.raaho.payment.model;

import lombok.Data;

@Data
public class BeneficiaryRequestModel {

	private String beneId;
	private String name;
	private String email;
	private String phone;
	private String bankAccount;
	private String ifsc;
	private String address1;
	private String city;
	private String state;
	private String pincode;
}
