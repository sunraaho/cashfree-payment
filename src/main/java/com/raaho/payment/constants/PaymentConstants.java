package com.raaho.payment.constants;

public class PaymentConstants {

	private PaymentConstants() {
	}

	// Cashfree Payment Constants
	public static final String SUCCESS = "SUCCESS";
	
	public static final String TOKEN = "token";

	public static final String DATA = "data";

	public static final String STATUS = "status";

	public static final String STATUS_CODE = "subCode";

	public static final String MESSAGE = "message";

	public static final String STATUS_MSG = "status : ";

	public static final String STATUS_CODE_MSG = "subCode : ";

	public static final String MESSAGE_MSG = "message : ";

	public static final String SLASH_N= "\n";

}
