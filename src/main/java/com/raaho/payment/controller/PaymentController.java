package com.raaho.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.raaho.payment.config.CashfreePaymentException;
import com.raaho.payment.constants.PaymentConstants;
import com.raaho.payment.model.BeneficiaryRequestModel;
import com.raaho.payment.service.CashfreePaymentService;

@RestController
@RequestMapping("cashfree/v1")
public class PaymentController {

	@Autowired
	CashfreePaymentService cashfreePaymentService;

	private String getExceptionBodyMsg(CashfreePaymentException cashfreePaymentException) {
		return PaymentConstants.STATUS_CODE_MSG + cashfreePaymentException.getStatusCode() + PaymentConstants.SLASH_N
				+ PaymentConstants.STATUS_MSG + cashfreePaymentException.getStatus() + PaymentConstants.SLASH_N
				+ PaymentConstants.MESSAGE_MSG + cashfreePaymentException.getMessage();
	}

	@GetMapping("/getToken")
	public ResponseEntity<String> authenticate() throws CashfreePaymentException {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(cashfreePaymentService.authenticate());
		} catch (CashfreePaymentException ex) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(getExceptionBodyMsg(ex));
		}
	}

	@PostMapping("/addBeneficiary")
	public ResponseEntity<String> addBeneficiary(@RequestParam("token") final String token,
			@RequestBody final BeneficiaryRequestModel requestModel) throws CashfreePaymentException {
		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(cashfreePaymentService.addBeneficiary(token, requestModel));
		} catch (CashfreePaymentException ex) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(getExceptionBodyMsg(ex));
		}
	}

	@PostMapping("/addBulkBeneficiaries")
	public ResponseEntity<String> addBulkBeneficiaries(@RequestParam("token") final String token)
			throws CashfreePaymentException {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(
					"Number of beneficiaries registered are : " + cashfreePaymentService.addBulkBeneficiaries(token));
		} catch (CashfreePaymentException ex) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(getExceptionBodyMsg(ex));
		}
	}
}
