package com.raaho.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@ComponentScan(basePackages = "com.raaho.payment")
@EntityScan(basePackages = "com.bookgo.payments.persistence")
@EnableJpaRepositories(basePackages = "com.bookgo.payments.persistence.repository")
@SpringBootApplication
public class CashfreePaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CashfreePaymentApplication.class, args);
	}
}
