package com.raaho.payment.config;

public class CashfreePaymentException extends Exception {

	private static final long serialVersionUID = 1L;
	private final String statusCode;
	private final String status;

	public CashfreePaymentException() {
		super();
		this.statusCode = null;
		this.status = null;
	}

	public CashfreePaymentException(String statusCode, String status, String message) {
		super(message);
		this.statusCode = statusCode;
		this.status = status;
	}

	public CashfreePaymentException(String message) {
		super(message);
		this.statusCode = null;
		this.status = null;
	}

	public CashfreePaymentException(Exception message) {
		super(message);
		this.statusCode = null;
		this.status = null;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public String getStatus() {
		return status;
	}

}
