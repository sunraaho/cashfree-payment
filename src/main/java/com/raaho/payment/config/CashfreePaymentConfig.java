package com.raaho.payment.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CashfreePaymentConfig {

	@Value("${cashfree.client.id}")
	private String clientId;

	@Value("${cashfree.client.secret}")
	private String clientSecret;

	/*
	 * @Bean public void initializeCashfreePayment() { Payouts payouts =
	 * Payouts.getInstance(Environment.TEST, clientId, clientSecret);
	 * payouts.init(); }
	 */

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

}
